using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Ambrogio.Plugins;

using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("Ambrogio.Tests")]
namespace Ambrogio
{
    /// <summary>
    /// Class representing a bot.
    /// </summary>
    public class Bot
    {
        public const string RELOAD_PLUGINS_MSG = "!RELOAD_PLUGINS";
        public const string RESET_STORE_MSG = "!RESET_STORE";
        public const string DEFAULT_USER = "XY";

        private static readonly Regex MessagePattern = new Regex("^([A-Z]{2}): (.+)$");

        private List<Plugin> plugins;
        private Dictionary<string, object> store;


        /// <param name="plugins">A list of plugins that should process messages received by the bot.</param>
        public Bot(List<Plugin> plugins = null)
        {
            this.plugins = plugins != null ? plugins : new List<Plugin>();
            this.store = new Dictionary<string, object>();
        }

        private void LoadPlugins()
        {
            foreach(Plugin plugin in this.plugins)
                plugin.Init(this);
        }

        internal (string sender, string text) SplitRawMessage(string rawMessage)
        {
            Match match = Bot.MessagePattern.Match(rawMessage);
            if (match.Success)
                return (match.Groups[1].Value, match.Groups[2].Value);
            else
                return (Bot.DEFAULT_USER, rawMessage);
        }

        public void Run()
        {
            this.LoadPlugins();

            while(true)
            {
                Console.Write("▶ ");
                string rawMessage = Console.ReadLine();
                (string sender, string text) = this.SplitRawMessage(rawMessage);
                Message message = new Message(text: text, sender: sender, date: DateTime.Now);
                this.HandleMessage(message);
            }
        }

        internal void HandleMessage(Message message)
        {
            // Handle debugging messages
            if (message.Text == Bot.RELOAD_PLUGINS_MSG)
            {
                this.HandleReloadPlugins();
                return;
            }

            if (message.Text == Bot.RESET_STORE_MSG)
            {
                this.HandleResetStore();
                return;
            }

            if (message.Sender != Bot.DEFAULT_USER)
                Console.WriteLine($"[👤 sent by {message.Sender}]");

            foreach (Plugin plugin in this.plugins)
                plugin.ReceiveMessage(this, message);
        }

        private void HandleReloadPlugins()
        {
            this.LoadPlugins();
        }

        private void HandleResetStore()
        {
            this.store = new Dictionary<string, object>();
        }

        /// <summary>Send some text to Ambrogio's output stream.</summary>
        public virtual void SendText(string text)
        {
            Console.WriteLine($"↪ {text}");
        }

        /// <summary>Store a value corresponding to a given key.</summary>
        /// <param name="key">The key to which this value corresponds.</param>
        public void StoreValue(string key, object value)
        {
            this.store[key] = value;
        }

        /// <summary>Retrieve the value associated to a given key.</summary>
        /// <param name="key">The key whose associated value is to be retrieved.</param>
        /// <param name="key">
        /// A default value that will be returned if no value had been saved for that key before.
        /// By default it will return the default value for the requested type.
        /// </param>
        /// <returns>The associated value, or defaultValue if no value had been saved for that key before.</returns>
        /// <exception cref="System.InvalidCastException">Thrown if the stored value cannot be cast to the requested output type</exception>
        public TV RetrieveValue<TV>(string key, TV defaultValue = default(TV))
        {
            try
            {
                return (TV) this.store[key];
            }
            catch (KeyNotFoundException)
            {
                return defaultValue;
            }
        }
    }
}