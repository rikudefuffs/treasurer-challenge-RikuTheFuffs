using System;

namespace Ambrogio
{
    /// <summary>
    /// Class representing a message received by Ambrogio.
    /// </summary>
    /// <param name="ambrogio">The bot that processes user requests.</param>
    public class Message
    {
        public Message(string text, string sender, DateTime date)
        {
            this.Text = text;
            this.Sender = sender;
            this.Date = date;
        }

        /// <value>The message body.</value>
        public string Text {get; private set;}

        /// <value>The message sender, represented by their initials (e.g. XY).</value>
        public string Sender {get; private set;}

        /// <value>The date and time when the message was received.</value>
        public DateTime Date {get; private set;}

        public override string ToString()
        {
            return this.Sender + ": " + this.Text + " - " + this.Date.ToString();
        }
    }
}
