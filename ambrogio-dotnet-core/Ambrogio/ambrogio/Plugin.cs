namespace Ambrogio.Plugins
{
    public interface Plugin
    {
        /// <summary>
        /// Perform any initialization operations when the bot starts.
        /// </summary>
        /// <param name="ambrogio">The bot that processes user requests.</param>
        void Init(Bot ambrogio);

        /// <summary>
        /// Process a message received by a user.
        /// </summary>
        /// <param name="ambrogio">The bot that processes user requests.</param>
        /// <param name="message">The received message.</param>
        void ReceiveMessage(Bot ambrogio, Message message);
    }
}
