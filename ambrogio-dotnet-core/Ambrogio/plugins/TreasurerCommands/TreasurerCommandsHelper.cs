﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ambrogio.Plugins.TreasurerCommands
{
    public class TreasurerCommandsHelper
    {
        public const string STANDARD_EVALUATING_MESSAGE = "Evaluating";
        public const string STANDARD_ERROR_MESSAGE = "";
        public const string STANDARD_SUCCESS_MESSAGE = "Done";

        public static readonly ICommand[] availableCommands =
        {
            new AddExpense(), new History(), new UnevenSplit(),
            new CreateGroup(), new EditGroup(), new Balance()
        };
    }

    public interface ICommand
    {
        void Initialize(Bot bot);

        /// <summary>
        /// the command structure is correct
        /// </summary>
        /// <param name="message"></param>
        /// <param name="output"></param>
        void Recognize(Message message, ref string output);

        /// <summary>
        /// the command data is valid (strings have the right length, numbers have the right number of decimals, there are no extra spaces between parts of the message, etc.)
        /// </summary>
        /// <param name="regexResult"></param>
        /// <param name="parametersWereCorrect"></param>
        /// <returns></returns>
        Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect);
        void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters);
        Match GetMatchFor(string input);
    }
}
