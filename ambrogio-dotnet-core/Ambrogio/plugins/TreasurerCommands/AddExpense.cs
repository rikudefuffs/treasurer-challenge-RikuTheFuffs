﻿using Ambrogio.plugins.Utils;
using Ambrogio.Plugins.TreasurerCommands.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Ambrogio.Plugins.TreasurerCommands
{
    /// <summary>
    /// Test cases:
    ///20|MD,LF "Take away"
    ///LQ: 10|GP "Ice cream"
    ///PB: 15|PB,AC
    /// </summary>
    public class AddExpense : ICommand
    {
        static readonly Regex groupMessagePattern = new Regex(@"^(([A-Z]{2}):\s)?(?<amount>\d{0,}(\.\d{1,2})?)\|(?<debtors>([A-Z]{2,12})(,[A-Z]{2,12})*)(\s""(?<reason>.*?)"")?$");
        static readonly Regex messagePattern = new Regex(@"^(([A-Z]{2}):\s)?(?<amount>\d{0,}(\.\d{1,2})?)\|(?<debtors>([A-Z]{2})(,[A-Z]{2})*)(\s""(?<reason>.*?)"")?$");
        public const string PARAMETER_AMOUNT = "amount";
        public const string PARAMETER_DEBTORS = "debtors";
        public const string PARAMETER_PAYMENT_REASON = "reason";
        Bot bot;

        public void Recognize(Message message, ref string output)
        {
            string decomposedMessageText = message.Text;
            Match groupMatch = groupMessagePattern.Match(decomposedMessageText);
            if (groupMatch.Success)
            {

                string[] groupDebtors = groupMatch.Groups[PARAMETER_DEBTORS].Value.Split(",")
                                                                                .Where(groupName => groupName.Length > 2)
                                                                                .ToArray();

                List<Data.Group> existingGroups = bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Data.Group>());
                foreach (string groupName in groupDebtors)
                {
                    Data.Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();
                    if (group == null)
                    {
                        output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                        return;
                    }
                    decomposedMessageText = decomposedMessageText.Replace(groupName, group.Decompose());
                }
            }
            Match match = GetMatchFor(decomposedMessageText);
            if (!match.Success) { return; }

            bool parametersWereCorrect = true;
            Dictionary<string, object> parameters = GetParameters(match, ref parametersWereCorrect);
            if (!parametersWereCorrect)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            Evaluate(message.Sender, message.Date, ref output, parameters);
        }

        public Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect)
        {
            try
            {
                decimal amount = Convert.ToDecimal(regexResult.Groups[PARAMETER_AMOUNT].Value, new System.Globalization.CultureInfo("en-US")); //culture is for the dot
                string[] debtors = regexResult.Groups[PARAMETER_DEBTORS].Value.Split(",");
                bool hasDuplicatedDebtors = debtors.GroupBy(x => x).Any(g => g.Count() > 1);
                if (hasDuplicatedDebtors)
                {
                    throw new ArgumentException();
                }
                string paymentReason = regexResult.Groups[PARAMETER_PAYMENT_REASON].Value;
                if (paymentReason.StartsWith("\"")
                || paymentReason.EndsWith("\"")
                || paymentReason.Contains("\"\""))
                {
                    throw new ArgumentException();
                }
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(PARAMETER_AMOUNT, amount);
                parameters.Add(PARAMETER_DEBTORS, debtors);
                parameters.Add(PARAMETER_PAYMENT_REASON, paymentReason);
                return parameters;
            }
            catch (Exception)
            {
                parametersWereCorrect = false;
                return null;
            }
        }

        public void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters)
        {
            try
            {
                decimal amount = (decimal)parameters[PARAMETER_AMOUNT];
                string[] debtors = (string[])parameters[PARAMETER_DEBTORS];
                string paymentReason = parameters.ContainsKey(PARAMETER_PAYMENT_REASON) ? (string)parameters[PARAMETER_PAYMENT_REASON] : string.Empty;
                StoreTransaction(sender, date, parameters);
                output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
            }
            catch (Exception)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
        }

        void StoreTransaction(string sender, DateTime date, Dictionary<string, object> parameters)
        {
            //all the transations ever (multiple users)
            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());

            //complete data of this transaction, to be added to the list
            Transaction newTransaction = new Transaction(sender, date, parameters);
            allStoredTransactions.Add(newTransaction);
            int newTransactionIndex = allStoredTransactions.Count - 1;

            //add the index to user's list of transactions for fast referencing later
            List<int> userTransactions = bot.RetrieveValue(sender, new List<int>());
            userTransactions.Add(newTransactionIndex);
            bot.StoreValue(sender, userTransactions);
            bot.StoreValue(Transaction.TRANSACTIONS_LIST, allStoredTransactions);
        }

        public Match GetMatchFor(string input)
        {
            return messagePattern.Match(input);
        }

        public void Initialize(Bot bot)
        {
            this.bot = bot;
        }
    }
}
