﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ambrogio.Plugins.TreasurerCommands.Data

{
    public class Group
    {
        public List<string> members { get; private set; }
        public string name { get; private set; }
        public int membersCount { get { return members.Count; } }

        public Group(string name)
        {
            this.name = name;
            members = new List<string>();
        }

        public bool AddMember(string memberName)
        {
            if (ContainsMember(memberName)) { return false; }
            members.Add(memberName);
            return true;
        }

        public bool RemoveMember(string memberName)
        {
            if (!ContainsMember(memberName)) { return false; }
            members.Remove(memberName);
            return true;
        }

        public bool ContainsMember(string memberName)
        {
            return members.Contains(memberName);
        }

        public string Decompose()
        {
            return string.Join(",", members.Select(x => x));
        }
    }
}