﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ambrogio.Plugins.TreasurerCommands.Data
{
    /// <summary>
    /// Holds data about a single transaction
    /// </summary>
    public class Transaction
    {
        static readonly Regex adderPattern = new Regex(@"(?<sum>[\+]\d{0,}(\.\d{1,2})?)");
        static readonly Regex multiplierPattern = new Regex(@"(?<mul>[\*]\d{0,}(\.\d{1,2})?)");
        public const string TRANSACTIONS_LIST = "Transactions";


        public DateTime date;
        Dictionary<string, object> data;
        Dictionary<string, decimal> moneyOwedByDebtors;

        decimal amount
        {
            get { return (decimal)data[AddExpense.PARAMETER_AMOUNT]; }
            set { data[AddExpense.PARAMETER_AMOUNT] = value; }
        }
        string paymentReason
        {
            get
            {
                return (string)data[AddExpense.PARAMETER_PAYMENT_REASON] == string.Empty ? string.Empty : " " + (string)data[AddExpense.PARAMETER_PAYMENT_REASON];
            }
        }
        public string payer { get; private set; }
        public string[] transactionDebtors { get { return moneyOwedByDebtors.Keys.ToArray(); } }

        public Transaction(string payer, DateTime date, Dictionary<string, object> data)
        {
            this.date = date;
            this.payer = payer;

            //data of this specific transaction, without payer and date of transaction
            this.data = data.ToDictionary(entry => entry.Key, entry => entry.Value);

            moneyOwedByDebtors = new Dictionary<string, decimal>();

            bool usingUnevenSplit = data.ContainsKey("Uneven");
            if (usingUnevenSplit)
            {
                Dictionary<string, string> unevenDebtors = (Dictionary<string, string>)this.data[AddExpense.PARAMETER_DEBTORS];
                Dictionary<string, decimal> quotas = unevenDebtors.ToDictionary(entry => entry.Key, entry => 1m);
                decimal flatAmountsToRemove = 0;
                decimal originalAmount = amount;

                foreach (var debtor in unevenDebtors)
                {
                    decimal multiplier;
                    decimal addedValue;
                    CalculateAdderAndMultiplier(debtor.Value, out multiplier, out addedValue);

                    if (addedValue > amount)
                    {
                        throw new ArgumentException();
                    }

                    //calculate the quota
                    quotas[debtor.Key] = multiplier;
                    if (debtor.Key == payer)
                    {
                        amount -= addedValue;
                    }
                    else
                    {
                        moneyOwedByDebtors[debtor.Key] = addedValue;
                    }
                    flatAmountsToRemove += addedValue;
                }
                decimal totalAmountWithoutAdditions = originalAmount - flatAmountsToRemove; //remove the flat amounts of everyone
                decimal perPersonAmount = totalAmountWithoutAdditions / quotas.Sum(x => x.Value); //calculate avg of the rest
                bool payerIsAlsoPayingForHimself = unevenDebtors.Where(x => (x.Key == payer)).Any();
                if (payerIsAlsoPayingForHimself)
                {
                    amount -= quotas[payer] * perPersonAmount;
                }
                unevenDebtors.Remove(payer);
                foreach (var debtor in unevenDebtors)
                {
                    moneyOwedByDebtors[debtor.Key] += quotas[debtor.Key] * perPersonAmount;
                }
            }
            else
            {
                string[] debtors = (string[])this.data[AddExpense.PARAMETER_DEBTORS];
                bool payerIsAlsoPayingForHimself = debtors.Where(x => (x == payer)).Any();
                if (payerIsAlsoPayingForHimself)
                {
                    decimal currentAmountAlreadySpentBySender = amount / debtors.Length;
                    amount -= currentAmountAlreadySpentBySender;

                    debtors = debtors.Where(x => x != payer).ToArray();
                    this.data[AddExpense.PARAMETER_DEBTORS] = debtors;
                }

                decimal perPersonAmount = amount / debtors.Length;
                foreach (string debtor in debtors)
                {
                    moneyOwedByDebtors[debtor] = perPersonAmount;
                }
            }
        }

        public decimal GetDebtOf(string debtor)
        {
            return moneyOwedByDebtors[debtor];
        }
        

        public string ToStringFor(string person)
        {
            if (person == payer)
            {
                return string.Format("{0}{1} - you get back {2}", date.ToString("dd/MM/yyyy"), paymentReason, amount.ToString("F", new System.Globalization.CultureInfo("en-US")));
            }
            return string.Format("{0}{1} - you pay back {2}", date.ToString("dd/MM/yyyy"), paymentReason, moneyOwedByDebtors[person].ToString("F", new System.Globalization.CultureInfo("en-US")));
        }

        public bool Involves(string person)
        {
            return (payer == person) || moneyOwedByDebtors.ContainsKey(person);
        }

        void CalculateAdderAndMultiplier(string modifiers, out decimal multiplier, out decimal addedValue)
        {
            multiplier = 1;
            addedValue = 0;
            if (string.IsNullOrEmpty(modifiers)) { return; }
            Match match = multiplierPattern.Match(modifiers);
            if (match.Success)
            {
                multiplier = Convert.ToDecimal(match.Groups["mul"].Value.TrimStart('*'), new System.Globalization.CultureInfo("en-US"));
            }
            match = adderPattern.Match(modifiers);
            if (match.Success)
            {
                addedValue = Convert.ToDecimal(match.Groups["sum"].Value.TrimStart('+'), new System.Globalization.CultureInfo("en-US"));
            }
        }
    }
}
