﻿using Ambrogio.Plugins.TreasurerCommands.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace Ambrogio.Plugins.TreasurerCommands
{
    /// <summary>
    /// Allows to add or remove members to/from a group
    /// </summary>
    class EditGroup : ICommand
    {
        static readonly Regex addMessagePattern = new Regex(@"^ADD (?<member>[A-Z]{2}) (?<group>[A-Z]{3,12})$");
        static readonly Regex deleteMessagePattern = new Regex(@"^DELETE (?<member>[A-Z]{2}) (?<group>[A-Z]{3,12})$");
        public const string PARAMETER_GROUP_NAME = "group";
        public const string PARAMETER_MEMBER = "member";
        Bot bot;

        public void Recognize(Message message, ref string output)
        {
            bool adding = true;
            Match match = GetMatchFor(message.Text);
            if (!match.Success)
            {
                match = deleteMessagePattern.Match(message.Text);
                if (!match.Success) { return; }
                adding = false;
            }

            bool parametersWereCorrect = true;
            Dictionary<string, object> parameters = GetParameters(match, ref parametersWereCorrect);
            if (!parametersWereCorrect)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            parameters.Add("adding", adding);
            Evaluate(message.Sender, message.Date, ref output, parameters);
        }

        public Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(PARAMETER_GROUP_NAME, regexResult.Groups[PARAMETER_GROUP_NAME].Value);
                parameters.Add(PARAMETER_MEMBER, regexResult.Groups[PARAMETER_MEMBER].Value);
                return parameters;
            }
            catch (Exception)
            {
                parametersWereCorrect = false;
                return null;
            }
        }

        public void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters)
        {
            bool adding = (bool)parameters["adding"];
            string groupName = (string)parameters[PARAMETER_GROUP_NAME];
            List<Data.Group> existingGroups = bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Data.Group>());
            Data.Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();
            if (group == null)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }

            string memberName = (string)parameters[PARAMETER_MEMBER];
            if (adding)
            {
                if (!group.AddMember(memberName))
                {
                    output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                    return;
                }
            }
            else
            {
                if (!group.RemoveMember(memberName))
                {
                    output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                    return;
                }
            }

            bot.StoreValue(CreateGroup.GROUPS_LIST, existingGroups);
            output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
        }

        public Match GetMatchFor(string input)
        {
            return addMessagePattern.Match(input);
        }

        public void Initialize(Bot bot)
        {
            this.bot = bot;
        }
    }
}
