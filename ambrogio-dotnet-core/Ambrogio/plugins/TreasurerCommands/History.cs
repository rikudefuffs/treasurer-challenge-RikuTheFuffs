﻿using Ambrogio.Plugins.TreasurerCommands.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace Ambrogio.Plugins.TreasurerCommands
{
    public class History : ICommand
    {
        static readonly Regex messagePattern = new Regex(@"^HISTORY$");
        Bot bot;

        public void Recognize(Message message, ref string output)
        {
            Match match = GetMatchFor(message.Text);
            if (!match.Success) { return; }

            bool parametersWereCorrect = true;
            Dictionary<string, object> parameters = GetParameters(match, ref parametersWereCorrect);
            if (!parametersWereCorrect)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            Evaluate(message.Sender, message.Date, ref output, parameters);
        }

        public Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect)
        {
            return null;
        }

        public void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters)
        {

            List<Transaction> transactionsInvolvingUser = bot.RetrieveValue("Transactions", new List<Transaction>())
                                                            .Where(transaction => transaction.Involves(sender))
                                                            .ToList();

            if (transactionsInvolvingUser.Count == 0)
            {
                output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
                return;
            }
            transactionsInvolvingUser.Sort((x, y) => DateTime.Compare(x.date, y.date));
            output = string.Join("\n", transactionsInvolvingUser.Select(x => x.ToStringFor(sender)));
        }

        public Match GetMatchFor(string input)
        {
            return messagePattern.Match(input);
        }

        public void Initialize(Bot bot)
        {
            this.bot = bot;
        }
    }
}