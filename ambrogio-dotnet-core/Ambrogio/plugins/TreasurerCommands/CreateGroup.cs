﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Ambrogio.Plugins.TreasurerCommands.Data;


namespace Ambrogio.Plugins.TreasurerCommands
{
    /// <summary>
    /// Allows for the creation of an empty group
    /// </summary>
    class CreateGroup : ICommand
    {
        static readonly Regex messagePattern = new Regex(@"^CREATE (?<name>[A-Z]{3,12})$");
        public const string PARAMETER_GROUP_NAME = "name";
        public const string GROUPS_LIST = "groups";
        Bot bot;

        public void Recognize(Message message, ref string output)
        {
            Match match = GetMatchFor(message.Text);
            if (!match.Success) { return; }

            bool parametersWereCorrect = true;
            Dictionary<string, object> parameters = GetParameters(match, ref parametersWereCorrect);
            if (!parametersWereCorrect)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            Evaluate(message.Sender, message.Date, ref output, parameters);
        }

        public Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add(PARAMETER_GROUP_NAME, regexResult.Groups[PARAMETER_GROUP_NAME].Value);
                return parameters;
            }
            catch (Exception)
            {
                parametersWereCorrect = false;
                return null;
            }
        }

        public void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters)
        {
            string groupName = (string)parameters[PARAMETER_GROUP_NAME];
            List<Data.Group> existingGroups = bot.RetrieveValue(GROUPS_LIST, new List<Data.Group>());
            Data.Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();
            if (group != null)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            existingGroups.Add(new Data.Group((string)parameters[PARAMETER_GROUP_NAME]));
            bot.StoreValue(GROUPS_LIST, existingGroups);
            output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
        }

        public Match GetMatchFor(string input)
        {
            return messagePattern.Match(input);
        }

        public void Initialize(Bot bot)
        {
            this.bot = bot;
        }
    }
}