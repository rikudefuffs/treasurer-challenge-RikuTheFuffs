﻿using Ambrogio.Plugins.TreasurerCommands.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using Ambrogio.plugins.Utils;

namespace Ambrogio.Plugins.TreasurerCommands
{
    /// <summary>
    /// This command displays the group's financial situation, reported as a set of transactions 
    /// that should be performed in order to settle all the debts within the group.
    /// The answer is a list of people, the user they owe money to, and the amount they owe.
    /// The plugin should always provide the same answer regardless of the person that asks for the balance.
    /// The balance should contain at least one line for each person that owes money
    /// </summary>
    public class Balance : ICommand
    {
        static readonly Regex messagePattern = new Regex(@"^BALANCE$");
        Bot bot;

        public void Recognize(Message message, ref string output)
        {
            Match match = GetMatchFor(message.Text);
            if (!match.Success) { return; }

            bool parametersWereCorrect = true;
            Dictionary<string, object> parameters = GetParameters(match, ref parametersWereCorrect);
            if (!parametersWereCorrect)
            {
                output = TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE;
                return;
            }
            Evaluate(message.Sender, message.Date, ref output, parameters);
        }

        public Dictionary<string, object> GetParameters(Match regexResult, ref bool parametersWereCorrect)
        {
            return null;
        }

        public void Evaluate(string sender, DateTime date, ref string output, Dictionary<string, object> parameters)
        {
            //generate a graph from all the transactions
            List<Transaction> transactions = bot.RetrieveValue("Transactions", new List<Transaction>());
            if (transactions.Count == 0)
            {
                output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
                return;
            }

            TransactionsGraph transactionsGraph = new TransactionsGraph(transactions);
            output = transactionsGraph.DisplaySituation();
            if (string.IsNullOrEmpty(output))
            {
                output = TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE;
            }
        }

        public Match GetMatchFor(string input)
        {
            return messagePattern.Match(input);
        }

        public void Initialize(Bot bot)
        {
            this.bot = bot;
        }
    }
}