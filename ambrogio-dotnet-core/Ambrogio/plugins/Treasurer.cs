using Ambrogio.Plugins.TreasurerCommands;

namespace Ambrogio.Plugins
{
    public class Treasurer : Plugin
    {
        public void Init(Bot ambrogio)
        {
            foreach (ICommand command in TreasurerCommandsHelper.availableCommands)
            {
                command.Initialize(ambrogio);
            }
        }

        public void ReceiveMessage(Bot ambrogio, Message message)
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            foreach (ICommand command in TreasurerCommandsHelper.availableCommands)
            {
                command.Recognize(message, ref result);
                if (result != TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE) { break; }
            }

            if (result == TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE
            || result == TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE) //command not recognized
            {
                return;
            }
            ambrogio.SendText(result);
        }
    }
}
