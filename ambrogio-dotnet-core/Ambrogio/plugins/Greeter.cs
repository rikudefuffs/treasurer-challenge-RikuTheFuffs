namespace Ambrogio.Plugins
{
    public class Greeter : Plugin
    {
        public void Init(Bot ambrogio){ }

        public void ReceiveMessage(Bot ambrogio, Message message)
        {
            if (message.Text == "hi")
                ambrogio.SendText($"hello {message.Sender}");
        }

    }
}
