﻿using Ambrogio.Plugins.TreasurerCommands.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ambrogio.plugins.Utils
{
    public class TransactionsGraph
    {
        public List<Vertex> vertices;
        public TransactionsGraph(List<Transaction> transactions)
        {
            vertices = new List<Vertex>();
            if (transactions == null) { return; }

            foreach (Transaction transaction in transactions)
            {
                Vertex creditorVertex = AddVertex(transaction.payer);
                foreach (string debtor in transaction.transactionDebtors)
                {
                    Vertex debtorVertex = AddVertex(debtor);
                    creditorVertex.AddCreditToward(debtorVertex, transaction.GetDebtOf(debtor));
                }
            }
            Optimize();
        }

        public Vertex AddVertex(string person)
        {
            Vertex v = vertices.Where(vertex => vertex.person == person).FirstOrDefault();
            if (v != null) { return v; }
            v = new Vertex(person);
            vertices.Add(v);
            return v;
        }

        public void Optimize()
        {
            Queue<Vertex> nodesToOptimize = new Queue<Vertex>(vertices);
            Vertex current;

            while (nodesToOptimize.Count > 0)
            {
                current = nodesToOptimize.Dequeue();
                current.OptimizeTransactions();
            }
        }

        public string DisplaySituation()
        {
            string output = string.Empty;
            foreach (Vertex vertex in vertices)
            {
                output += vertex.DisplayDebtors();
            }
            if (string.IsNullOrEmpty(output)) { return output; }
            return output.Remove(output.Length - 1, 1);
        }

        public void DisplaySituationOf(string person)
        {
            Vertex vertex = vertices.Where(v => v.person == person).FirstOrDefault();
            vertex.DisplayDebtors();
        }
    }
    public class Vertex
    {
        public string person { get; set; }
        public Dictionary<Vertex, Edge> debtors;
        public bool hasDebtors { get { return debtors.Count > 0; } }
        public bool hasCreditors { get { return debtors.Count > 0; } }
        public bool IsCreditorOf(Vertex anotherPerson) { return debtors.ContainsKey(anotherPerson); }
        /// <summary>
        /// Adds creditor
        /// </summary>
        /// <param name="debtor"></param>
        /// <param name="amount"></param>
        public void AddCreditToward(Vertex debtor, decimal amount)
        {
            if (IsCreditorOf(debtor))
            {
                bool reversedOrNullifiedDebt = debtors[debtor].AddAmount(amount);
                if (reversedOrNullifiedDebt)
                {
                    decimal newDebtAmount = debtors[debtor].debtAmount;
                    debtors.Remove(debtor);
                    if (newDebtAmount != 0) //reversed
                    {
                        debtor.AddCreditToward(this, newDebtAmount);
                    }
                }
            }
            else if (debtor.IsCreditorOf(this))
            {
                debtor.AddCreditToward(this, -amount);
            }
            else
            {
                debtors.Add(debtor, new Edge(this, debtor, amount));
            }
        }

        public string DisplayDebtors()
        {
            string output = string.Empty;
            foreach (var debtor in debtors)
            {
                output += string.Format("{0} owes {1} {2}\n", debtor.Key.person, person, debtor.Value.debtAmount);
            }
            return output;
        }


        public Vertex(string person)
        {
            this.person = person;
            debtors = new Dictionary<Vertex, Edge>();
        }

        public void OptimizeTransactions()
        {
            List<Vertex> debtorsList = debtors.Keys.ToList();

            for (int i = 0; i < debtorsList.Count; i++)
            {
                Vertex directDebtor = debtorsList[i];
                if (!directDebtor.hasDebtors) { continue; }

                decimal debtAmountTowardsCreditor = debtors[directDebtor].debtAmount;
                List<Vertex> subDebtorsList = directDebtor.debtors.Keys.ToList();
                for (int j = 0; j < subDebtorsList.Count; j++)
                {
                    Vertex indirectDebtor = subDebtorsList[j];
                    decimal debtAmountTowardsDebtor = directDebtor.debtors[indirectDebtor].debtAmount;
                    if (debtAmountTowardsCreditor == debtAmountTowardsDebtor)
                    {
                        if (debtors.ContainsKey(indirectDebtor))
                        {
                            Edge subDebtorTowardsCreditorTransaction = debtors[indirectDebtor];
                            subDebtorTowardsCreditorTransaction.AddAmount(debtAmountTowardsDebtor);
                        }
                        else
                        {
                            AddCreditToward(indirectDebtor, debtAmountTowardsCreditor);
                            debtorsList.Add(indirectDebtor);
                        }
                        //Remove old transactions
                        directDebtor.debtors.Remove(indirectDebtor);
                        debtors.Remove(directDebtor);
                    }
                    else
                    {
                        if (debtors.ContainsKey(indirectDebtor))
                        {
                            Edge subDebtorTowardsCreditorTransaction = debtors[indirectDebtor];
                            Edge subDebtorTowardsDebtorTransaction = directDebtor.debtors[indirectDebtor];

                            if (debtAmountTowardsDebtor > debtAmountTowardsCreditor)
                            {
                                subDebtorTowardsDebtorTransaction.AddAmount(-debtAmountTowardsCreditor);
                                debtors.Remove(directDebtor);
                                subDebtorTowardsCreditorTransaction.AddAmount(debtAmountTowardsCreditor);
                            }
                            else
                            {
                                AddCreditToward(directDebtor, -debtAmountTowardsDebtor);
                                directDebtor.debtors.Remove(indirectDebtor);
                                subDebtorTowardsCreditorTransaction.AddAmount(debtAmountTowardsDebtor);
                            }
                        }
                    }
                }
            }
        }
    }

    public class Edge
    {
        public Vertex creditor { get; set; }
        public Vertex debtor { get; set; }
        public decimal debtAmount { get; set; }

        public Edge(Vertex creditor, Vertex debtor, decimal debtAmount)
        {
            this.creditor = creditor;
            this.debtor = debtor;
            this.debtAmount = debtAmount;
        }

        public bool AddAmount(decimal amount)
        {
            debtAmount += amount;
            if (debtAmount > 0) { return false; }
            if (debtAmount == 0)
            {
                //destroy
                return true;
            }
            ReverseDirection();
            return true;
        }

        public void ReverseDirection()
        {
            Vertex temp = creditor;
            creditor = debtor;
            debtor = temp;
            debtAmount = debtAmount * -1;
        }
    }
}
