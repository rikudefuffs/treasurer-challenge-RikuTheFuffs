﻿using System;
using System.Collections.Generic;
using Ambrogio.Plugins;

namespace Ambrogio
{
    class TreasurerChallenge
    {
        static void Main(string[] args)
        {
            var plugins = new List<Plugin>(){
                new Greeter(),
                new Treasurer()
            };

            Bot ambrogio = new Bot(plugins);
            ambrogio.Run();
        }
    }
}

