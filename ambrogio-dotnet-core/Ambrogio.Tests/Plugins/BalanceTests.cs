﻿using System;
using Xunit;
using System.Collections.Generic;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;
using System.Linq;
using Ambrogio.Plugins.TreasurerCommands.Data;
using Ambrogio.plugins.Utils;

namespace Ambrogio.Tests.Plugins
{
    public class BalanceTests
    {
        Treasurer plugin;
        BotMock bot;

        public BalanceTests()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void GraphInitialize_CorrectInput_Success()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            Message message = new Message(sender: "AA", text: "20|GG,BB \"Take away\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "40|AA,BB \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);

            Assert.Equal(3, transactionsGraph.vertices.Count);
        }

        [Fact]
        public void GraphAddDebt_Cumulates_Success()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            Message message = new Message(sender: "AA", text: "20|GG,BB \"Take away\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "AA", text: "40|GG,BB \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);
            result = transactionsGraph.DisplaySituation();

            Assert.Equal("GG owes AA 30\nBB owes AA 30", result);
        }

        [Fact]
        public void GraphReverseDebt_CorrectAmount_Success()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            Message message = new Message(sender: "AA", text: "20|GG \"Take away\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "30|AA \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);
            result = transactionsGraph.DisplaySituation();

            Assert.Equal("AA owes GG 10", result);
        }

        [Fact]
        public void GraphOptimizeTransactions_AllEven_Success()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            Message message = new Message(sender: "AA", text: "20|GG,BB \"Take away\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "20|AA,BB \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "BB", text: "20|GG,AA \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            ICommand balance = new Balance();
            balance.Initialize(bot);
            message = new Message(sender: "BB", text: "BALANCE", date: DateTime.Now);
            balance.Recognize(message, ref result);

            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, result);
        }

        [Fact]
        public void GraphOptimizeTransactions_CorrectAmount_Success()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            Message message = new Message(sender: "AA", text: "10|BB \"Take away\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "20|AA,BB \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "10|BB \"Pizza\"", date: DateTime.Now);
            addExpense.Recognize(message, ref result);


            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);
            result = transactionsGraph.DisplaySituation();

            Assert.Equal("BB owes GG 30", result);
        }

        [Fact]
        public void OptimizeTransactions_LinearReferences_CollapseIntoOne()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);


            Message message = new Message(sender: "AA", text: "10|BB", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "BB", text: "10|CC", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "CC", text: "10|DD", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "DD", text: "10|EE", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "EE", text: "10|FF", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);
            result = transactionsGraph.DisplaySituation();

            Assert.Equal("FF owes AA 10", result);
        }

        [Fact]
        public void OptimizeTransactions_ComplexButPerfectGraph_CollapseIntoTwo()
        {
            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);


            Message message = new Message(sender: "AA", text: "10|BB", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "10|AA", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "GG", text: "20|BB", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "BB", text: "25|PP", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "BB", text: "20|ZZ", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            message = new Message(sender: "ZZ", text: "5|PP", date: DateTime.Now);
            addExpense.Recognize(message, ref result);

            List<Transaction> allStoredTransactions = bot.RetrieveValue(Transaction.TRANSACTIONS_LIST, new List<Transaction>());
            TransactionsGraph transactionsGraph = new TransactionsGraph(allStoredTransactions);
            result = transactionsGraph.DisplaySituation();

            Assert.Equal("ZZ owes BB 15\nPP owes GG 30", result);
        }
    }
}
