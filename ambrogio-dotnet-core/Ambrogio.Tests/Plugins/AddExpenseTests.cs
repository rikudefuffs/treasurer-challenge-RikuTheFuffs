﻿using System;
using Xunit;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;
using System.Collections.Generic;

namespace Ambrogio.Tests.Plugins
{
    public class AddExpenseTests
    {
        Treasurer plugin;
        BotMock bot;

        public AddExpenseTests()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void AddExpense_InputAllParams_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: "20|MD,LF \"Take away\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);

            Assert.Equal(20.00m, (decimal)parameters[AddExpense.PARAMETER_AMOUNT]);
            Assert.Equal(2, ((string[])parameters[AddExpense.PARAMETER_DEBTORS]).Length);
            Assert.Equal("MD", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[0]);
            Assert.Equal("LF", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[1]);
            Assert.Equal("Take away", (string)parameters[AddExpense.PARAMETER_PAYMENT_REASON]);
        }

        [Fact]
        public void AddExpense_InputAmountAndDebtors_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: "20|MD,LF,XX", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);

            Assert.Equal(20.00m, (decimal)parameters[AddExpense.PARAMETER_AMOUNT]);
            Assert.Equal(3, ((string[])parameters[AddExpense.PARAMETER_DEBTORS]).Length);
            Assert.Equal("MD", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[0]);
            Assert.Equal("LF", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[1]);
            Assert.Equal("XX", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[2]);
        }

        [Fact]
        public void AddExpense_InputAmountAndDebtor_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: "20|MD", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);

            Assert.Equal(20.00m, (decimal)parameters[AddExpense.PARAMETER_AMOUNT]);
            Assert.Single((string[])parameters[AddExpense.PARAMETER_DEBTORS]);
            Assert.Equal("MD", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[0]);
        }

        [Fact]
        public void AddExpense_InputFourDigitAmountAndDebtor_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: "20.50|MD", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);

            Assert.Equal(20.50m, (decimal)parameters[AddExpense.PARAMETER_AMOUNT]);
            Assert.Single((string[])parameters[AddExpense.PARAMETER_DEBTORS]);
            Assert.Equal("MD", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[0]);
        }

        [Fact]
        public void AddExpense_InputNoDigitAmountAndDebtor_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: ".50|MD", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);

            Assert.Equal(0.50m, (decimal)parameters[AddExpense.PARAMETER_AMOUNT]);
            Assert.Single((string[])parameters[AddExpense.PARAMETER_DEBTORS]);
            Assert.Equal("MD", ((string[])parameters[AddExpense.PARAMETER_DEBTORS])[0]);
        }

        [Fact]
        public void AddExpense_InputWrongNumberSeparator_Fails()
        {
            Message message = new Message(sender: "RA", text: "20,50|MD", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);
            Assert.False(parametersWereCorrect);
        }

        [Fact]
        public void AddExpense_InputWrongNumberSign_Fails()
        {
            Message message = new Message(sender: "RA", text: "-0.50|MD", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            bool parametersWereCorrect = false;
            Dictionary<string, object> parameters = addExpense.GetParameters(addExpense.GetMatchFor(message.Text), ref parametersWereCorrect);
            Assert.False(parametersWereCorrect);
        }

        [Fact]
        public void AddExpense_RecognizeCorrectMessage_Success()
        {
            Message message = new Message(sender: "RA", text: "0.50|MD \"Taxi run\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, result);
        }

        [Fact]
        public void AddExpense_RecognizeWrongMessage_Fails()
        {
            Message message = new Message(sender: "RA", text: "1,50|AA \"Taxi run\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.NotEqual(TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE, result);
        }

        [Fact]
        public void AddExpense_RecognizeWrongMessage_CommandNotRecognized()
        {
            Message message = new Message(sender: "RA", text: "0.50| \"Taxi run\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE, result);
        }

        [Fact]
        public void AddExpense_DuplicatedDebitors_Fails()
        {
            Message message = new Message(sender: "RA", text: "0.50|GG,GG \"TaxiRun\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE, result);
        }

        [Fact]
        public void AddExpense_MessageHasDoubleQuotes_Fails()
        {
            Message message = new Message(sender: "RA", text: "0.50|GG,GA \"Tax\"\"iRun\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_ERROR_MESSAGE, result);
        }

        [Fact]
        public void AddExpense_RecognizeCorrectGroupMessage_Success()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "SS";
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);
            Message message = new Message(sender: "LQ", text: "ADD " + memberName + " " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            groupName = "PIZZAIOLI";
            memberName = "GG";
            creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);

            message = new Message(sender: "LQ", text: "ADD " + memberName + " " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);
            memberName = "OO";
            message = new Message(sender: "LQ", text: "ADD " + memberName + " " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            message = new Message(sender: "RA", text: "150|XX,SUSHILOVERS,MD,PIZZAIOLI \"Capperi\"", date: DateTime.Now);
            ICommand addExpense = new AddExpense();
            addExpense.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            addExpense.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, result);
        }
    }
}
