﻿using System;
using Xunit;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;
using System.Collections.Generic;
using Ambrogio.Plugins.TreasurerCommands.Data;

namespace Ambrogio.Tests.Plugins
{
    public class HistoryTests
    {
        Treasurer plugin;
        BotMock bot;

        public HistoryTests()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void NoTransactions_GetsStandardOutput()
        {
            Message message = new Message(sender: "XX", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);
            Assert.Single(bot.Logs);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, bot.Logs[0]);
        }

        [Fact]
        public void HasTransactionsButNotForThisUser_GetsStandardOutput()
        {
            Message transactionMessage = new Message(sender: "XY", text: "20|MD,LF \"Take away\"", date: DateTime.Now);
            Message historyMessage = new Message(sender: "BB", text: "HISTORY", date: DateTime.Now);

            plugin.ReceiveMessage(bot, transactionMessage);
            plugin.ReceiveMessage(bot, historyMessage);
            Assert.Equal(2, bot.Logs.Count);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, bot.Logs[1]);
        }

        [Fact]
        public void HasSingleTransactionForThisUser_GetsTransactions()
        {
            Message transactionMessage = new Message(sender: "KK", text: "20|MD,LF \"Take away\"", date: new DateTime(2019, 10, 11));
            Message historyMessage = new Message(sender: "KK", text: "HISTORY", date: DateTime.Now);

            plugin.ReceiveMessage(bot, transactionMessage);
            plugin.ReceiveMessage(bot, historyMessage);
            Assert.Equal(2, bot.Logs.Count);
            Assert.Equal("11/10/2019 Take away - you get back 20.00", bot.Logs[1]);
        }

        [Fact]
        public void HasTransactionsForPayer_GetsTransactions()
        {
            Message transactionMessage = new Message(sender: "XX", text: "20|MD,LF \"Take away\"", date: new DateTime(2019, 8, 11));
            plugin.ReceiveMessage(bot, transactionMessage);

            transactionMessage = new Message(sender: "XZ", text: "10.88|MD,LF \"Pizza\"", date: new DateTime(2019, 10, 11));
            plugin.ReceiveMessage(bot, transactionMessage);

            transactionMessage = new Message(sender: "AX", text: "5.22|MD,LF \"Taxi\"", date: new DateTime(2019, 10, 11));
            plugin.ReceiveMessage(bot, transactionMessage);

            transactionMessage = new Message(sender: "XX", text: "40|XX,AX,MD,LF", date: new DateTime(2019, 11, 11));
            plugin.ReceiveMessage(bot, transactionMessage);

            Message historyMessage = new Message(sender: "XX", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);
            Assert.Equal(5, bot.Logs.Count);
            Assert.Equal("11/08/2019 Take away - you get back 20.00\n11/11/2019 - you get back 30.00", bot.Logs[4]);
        }

        [Fact]
        public void HasTransactionsForDebtor_GetsTransactions()
        {
            Message transactionMessage = new Message(sender: "XX", text: "20|MD,LF \"Take away\"", date: new DateTime(2019, 10, 11, 22, 59, 59));
            plugin.ReceiveMessage(bot, transactionMessage);

            transactionMessage = new Message(sender: "XX", text: "42|XX,AX,MD \"stuff\"", date: new DateTime(2019, 10, 11, 20, 59, 59));
            plugin.ReceiveMessage(bot, transactionMessage);

            transactionMessage = new Message(sender: "LF", text: "40|XX,AX \"Other stuff\"", date: new DateTime(2019, 10, 11, 18, 59, 59));
            plugin.ReceiveMessage(bot, transactionMessage);

            Message historyMessage = new Message(sender: "LF", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);
            Assert.Equal(4, bot.Logs.Count);
            Assert.Equal("11/10/2019 Other stuff - you get back 40.00\n11/10/2019 Take away - you pay back 10.00", bot.Logs[3]);
        }
    }
}
