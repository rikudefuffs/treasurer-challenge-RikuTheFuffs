﻿using System;
using Xunit;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;
using System.Collections.Generic;

namespace Ambrogio.Tests.Plugins
{
    public class UnevenSplitTests
    {
        Treasurer plugin;
        BotMock bot;

        public UnevenSplitTests()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void InputAllParamsWithMixedModifiers_Success()
        {
            Message message = new Message(sender: "LQ", text: "62|MM+2.50*3,LQ*2+12,FP \"Taxi\"", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);
            Assert.Single(bot.Logs);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, bot.Logs[0]);
        }

        [Fact]
        public void InputAllParamsWithWrongModifiers_Fail()
        {
            Message message = new Message(sender: "LQ", text: "30|MM+100,LQ,FP \"Taxi\"", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);
            Assert.Empty(bot.Logs);
        }

        [Fact]
        public void InputAllParamsWithMixedModifiersAndSeesHistory_Success()
        {
            Message message = new Message(sender: "MM", text: "62|MM*3,LQ*2,FP+2 \"Taxi\"", date: new DateTime(2019, 10, 11, 12, 33, 44));
            plugin.ReceiveMessage(bot, message);

            message = new Message(sender: "MM", text: "62|MM+2*3,LQ*2,FP", date: new DateTime(2019, 10, 11, 13, 33, 44));
            plugin.ReceiveMessage(bot, message);

            Message historyMessage = new Message(sender: "MM", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);

            Assert.Equal("11/10/2019 Taxi - you get back 32.00\n11/10/2019 - you get back 30.00", bot.Logs[2]);
        }

        [Fact]
        public void InputAndSeeHistoryForDebtor_Success()
        {
            Message message = new Message(sender: "MM", text: "62|MM*3,LQ*2,FP+2 \"Taxi\"", date: new DateTime(2019, 10, 11, 12, 33, 44));
            plugin.ReceiveMessage(bot, message);

            message = new Message(sender: "XX", text: "62|MM+2*3,LQ*2,FP", date: new DateTime(2019, 10, 11, 13, 33, 44));
            plugin.ReceiveMessage(bot, message);

            Message historyMessage = new Message(sender: "MM", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);

            Assert.Equal("11/10/2019 Taxi - you get back 32.00\n11/10/2019 - you pay back 32.00", bot.Logs[2]);
        }

        [Fact]
        public void InputAndSeeHistoryForNonPayer_Success()
        {
            Message message = new Message(sender: "MM", text: "62|MM*3,LQ*2,FP+2 \"Taxi\"", date: new DateTime(2019, 10, 11, 12, 33, 44));
            plugin.ReceiveMessage(bot, message);

            message = new Message(sender: "XX", text: "62|MM+2*3,LQ*2,FP", date: new DateTime(2019, 10, 11, 13, 33, 44));
            plugin.ReceiveMessage(bot, message);

            Message historyMessage = new Message(sender: "LQ", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);

            Assert.Equal("11/10/2019 Taxi - you pay back 20.00\n11/10/2019 - you pay back 20.00", bot.Logs[2]);
        }

        [Fact]
        public void AddExpense_RecognizeCorrectGroupMessage_Success()
        {
            CreateGroup(bot, "SUSHILOVERS");
            AddMember(bot, "LQ", "SUSHILOVERS");
            AddMember(bot, "DD", "SUSHILOVERS");
            AddMember(bot, "UU", "SUSHILOVERS");

            CreateGroup(bot, "PIZZAIOLI");
            AddMember(bot, "MD", "PIZZAIOLI");
            AddMember(bot, "OO", "PIZZAIOLI");

            Message message = new Message(sender: "RA", text: "123|SUSHILOVERS+6,PIZZAIOLI*2", date: new DateTime(2019, 10, 15, 13, 33, 44));
            ICommand unevenSplit = new UnevenSplit();
            unevenSplit.Initialize(bot);

            string result = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;
            unevenSplit.Recognize(message, ref result);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, result);

            Message historyMessage = new Message(sender: "LQ", text: "HISTORY", date: DateTime.Now);
            plugin.ReceiveMessage(bot, historyMessage);
            Assert.Equal("15/10/2019 - you pay back 21.00", bot.Logs[7]);
        }

        void CreateGroup(Bot b, string groupName)
        {
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(b, creationMessage);
        }

        void AddMember(Bot b, string memberName, string groupName)
        {
            Message message = new Message(sender: "LQ", text: string.Format("ADD {0} {1}", memberName, groupName), date: DateTime.Now);
            plugin.ReceiveMessage(b, message);
        }
    }
}
