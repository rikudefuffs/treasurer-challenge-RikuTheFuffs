﻿using System;
using Xunit;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;
using System.Collections.Generic;
using System.Linq;
using Ambrogio.Plugins.TreasurerCommands.Data;

namespace Ambrogio.Tests.Plugins
{
    public class GroupTests
    {
        Treasurer plugin;
        BotMock bot;

        public GroupTests()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void Create_WithoutName_IgnoresMessage()
        {
            Message message = new Message(sender: "LQ", text: "CREATE", date: DateTime.Now);
            string output = TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE;

            ICommand createGroup = new CreateGroup();
            createGroup.Initialize(bot);
            createGroup.Recognize(message, ref output);

            Assert.Equal(TreasurerCommandsHelper.STANDARD_EVALUATING_MESSAGE, output);
        }

        [Fact]
        public void Create_WithSmallName_Fail()
        {
            string groupName = "GI";
            Message message = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);

            plugin.ReceiveMessage(bot, message);

            Assert.DoesNotContain(groupName, bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>().Select(x => x.name)));
        }

        [Fact]
        public void Create_WithLargeName_Fail()
        {
            string groupName = "GIGIOLOPAPSDFSPDFDS";
            Message message = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);

            plugin.ReceiveMessage(bot, message);

            Assert.DoesNotContain(groupName, bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>()).Select(x => x.name));
        }

        [Fact]
        public void Create_WithCorrectSizedName_CreatesGroup()
        {
            string groupName = "GIGIOLO";
            Message message = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);

            plugin.ReceiveMessage(bot, message);

            Assert.Contains(groupName, bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>()).Select(x => x.name));
        }

        [Fact]
        public void Create_ExistingGroup_Fail()
        {
            string groupName = "SUSHILOVERS";
            Message message = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);

            plugin.ReceiveMessage(bot, message);
            Assert.Single(bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>()));
            plugin.ReceiveMessage(bot, message);
            Assert.Single(bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>()));
        }

        [Fact]
        public void Add_NewMemberToNonExistingGroup_Fail()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";

            Message message = new Message(sender: "LQ", text: "ADD " + memberName + " " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            Assert.Empty(bot.Logs);
        }

        [Fact]
        public void Add_NewMemberToExistingGroup_Success()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);

            Message message = new Message(sender: "LQ", text: "ADD " + memberName + " SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            List<Group> existingGroups = bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>());
            Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();

            Assert.True(group.ContainsMember(memberName));
        }

        [Fact]
        public void Add_ExistingMemberToExistingGroup_Fail()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);

            Message message = new Message(sender: "LQ", text: "ADD " + memberName + " SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            List<Group> existingGroups = bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>());
            Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();

            plugin.ReceiveMessage(bot, message);

            Assert.Equal(1, group.membersCount);
        }

        [Fact]
        public void Delete_MemberFromNonExistingGroup_Fail()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";

            Message message = new Message(sender: "LQ", text: "DELETE " + memberName + " " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            Assert.Empty(bot.Logs);
        }

        [Fact]
        public void Delete_NewMemberFromExistingGroup_Fail()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);

            Message message = new Message(sender: "LQ", text: "DELETE " + memberName + " SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, message);

            Assert.Single(bot.Logs);
            Assert.Equal(TreasurerCommandsHelper.STANDARD_SUCCESS_MESSAGE, bot.Logs[0]);
        }

        [Fact]
        public void Delete_ExistingMemberFromExistingGroup_Success()
        {
            string groupName = "SUSHILOVERS";
            string memberName = "GG";
            Message creationMessage = new Message(sender: "LQ", text: "CREATE " + groupName, date: DateTime.Now);
            plugin.ReceiveMessage(bot, creationMessage);

            List<Group> existingGroups = bot.RetrieveValue(CreateGroup.GROUPS_LIST, new List<Group>());
            Group group = existingGroups.Where(x => (x.name == groupName)).FirstOrDefault();

            Message addMessage = new Message(sender: "LQ", text: "ADD " + memberName + " SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, addMessage);

            addMessage = new Message(sender: "LQ", text: "ADD XX SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, addMessage);

            Assert.Equal(2, group.membersCount);

            Message deleteMessage = new Message(sender: "LQ", text: "DELETE " + memberName + " SUSHILOVERS", date: DateTime.Now);
            plugin.ReceiveMessage(bot, deleteMessage);

            Assert.Equal(1, group.membersCount);
        }
    }
}
