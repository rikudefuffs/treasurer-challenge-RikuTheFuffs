using System;
using Xunit;
using Ambrogio.Plugins;

namespace Ambrogio.Tests
{
    public class GreeterTest
    {
        Greeter plugin;
        BotMock bot;

        public GreeterTest()
        {
            this.plugin = new Greeter();
            this.bot = new BotMock();
        }

        [Fact]
        public void TestNoAnswer()
        {
            var message = new Message(sender: "RA", text: "yo", date: DateTime.Now);
            this.plugin.ReceiveMessage(this.bot, message);
            Assert.Empty(this.bot.Logs);
        }

        
        [Fact]
        public void TestHelloRA()
        {
            var message = new Message(sender: "RA", text: "hi", date: DateTime.Now);
            this.plugin.ReceiveMessage(this.bot, message);
            Assert.Single(this.bot.Logs);
            Assert.Equal("hello RA", this.bot.Logs[0]);
        }
    }
}
