using System;
using System.Collections.Generic;
using Ambrogio.Plugins;
using Xunit;


namespace Ambrogio.Tests
{
    public class BotTest
    {
        BotMock bot;
        PluginMock plugin;

        public BotTest()
        {
            this.plugin = new PluginMock();
            this.bot = new BotMock(new List<Plugin>(){ this.plugin });
        }

        [Fact]
        public void TestStore()
        {
            this.bot.StoreValue("a-key", 42);
            Assert.Equal(42, this.bot.RetrieveValue<int>("a-key"));
        }

        [Fact]
        public void TestStoreDefaultValue()
        {
            Assert.Equal(0, this.bot.RetrieveValue<int>("a-key"));
            Assert.Equal(42, this.bot.RetrieveValue<int>("a-key", 42));
            Assert.Null(this.bot.RetrieveValue<object>("a-key"));
            Assert.Null(this.bot.RetrieveValue<string>("a-key"));
            Assert.Equal("hey", this.bot.RetrieveValue<string>("a-key", "hey"));
        }

        [Fact]
        public void TestOverwriteStoreValue()
        {
            this.bot.StoreValue("a-key", 42);
            Assert.Equal(42, this.bot.RetrieveValue<int>("a-key"));
            this.bot.StoreValue("a-key", 43);
            Assert.Equal(43, this.bot.RetrieveValue<int>("a-key"));
        }

        [Fact]
        public void TestStoreInvalidCast()
        {
            this.bot.StoreValue("a-key", 42);
            Assert.Throws<InvalidCastException>(() => this.bot.RetrieveValue<string>("a-key"));
        }

        [Fact]
        public void MessageReceivedByPlugin()
        {
            var message = new Message(sender: "RA", text: "hi", date: DateTime.Now);
            this.bot.HandleMessage(message);

            Assert.NotNull(this.plugin.Message);
            Assert.Equal("RA", this.plugin.Message.Sender);
            Assert.Equal("hi", this.plugin.Message.Text);
        }

        [Fact]
        public void TestReloadPlugins()
        {
            var message = new Message(sender: "RA", text: BotMock.RELOAD_PLUGINS_MSG, date: DateTime.Now);
            this.bot.HandleMessage(message);
            Assert.Equal(1, this.plugin.InitCalls);
            Assert.Null(this.plugin.Message);
        }

        [Fact]
        public void TestResetStore()
        {
            var message = new Message(sender: "RA", text: BotMock.RESET_STORE_MSG, date: DateTime.Now);
            this.bot.StoreValue("a-key", "hi");
            this.bot.HandleMessage(message);            
            Assert.Null(this.bot.RetrieveValue<string>("a-key"));
            Assert.Null(this.plugin.Message);
        }

        [Theory]
        [InlineData("RA: Hello!", "RA", "Hello!")]
        [InlineData("Hello!", Bot.DEFAULT_USER, "Hello!")]
        [InlineData("TOOLONGUSER: hi", Bot.DEFAULT_USER, "TOOLONGUSER: hi")]
        [InlineData("A: hi", Bot.DEFAULT_USER, "A: hi")]
        public void TestSplitRawMessage(string rawMessage, string expectedSender, string expectedText)
        {
            (string sender, string text) = this.bot.SplitRawMessage(rawMessage);
            Assert.Equal(expectedSender, sender);
            Assert.Equal(expectedText, text);
        }

    }
}
