using System;
using Xunit;
using Ambrogio.Plugins;
using Ambrogio.Plugins.TreasurerCommands;

namespace Ambrogio.Tests
{
    public class TreasurerTest
    {
        Treasurer plugin;
        BotMock bot;

        public TreasurerTest()
        {
            this.bot = new BotMock();
            this.plugin = new Treasurer();
            this.plugin.Init(bot);
        }

        [Fact]
        public void AddExpense_InputAllParams_GetsAllParams()
        {
            Message message = new Message(sender: "RA", text: "20|MD,LF \"Take away\"", date: DateTime.Now);
            this.plugin.ReceiveMessage(this.bot, message);
            Assert.Single(this.bot.Logs);
            Assert.Equal("Done", this.bot.Logs[0]);
        }
    }
}
