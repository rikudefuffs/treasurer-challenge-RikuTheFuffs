using System;
using Xunit;
using Ambrogio.Plugins;

namespace Ambrogio.Tests
{
    public class PluginMock : Plugin
    {
        public int InitCalls {get; set;} = 0;
        public Message Message {get; set;}

        public void Init(Bot ambrogio)
        {
            this.InitCalls++;
        }

        public void ReceiveMessage(Bot ambrogio, Message message)
        {
            this.Message = message;
        }
    }
}
