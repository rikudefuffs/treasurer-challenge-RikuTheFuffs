using System.Collections.Generic;
using Ambrogio;
using Ambrogio.Plugins;

namespace Ambrogio.Tests
{
    class BotMock : Bot
    {
        public BotMock(List<Plugin> plugins = null) : base(plugins)
        {
            this.Logs = new List<string>();
        }

        public List<string> Logs {get; set;}

        public override void SendText(string text)
        {
            this.Logs.Add(text);
        }
    }
}