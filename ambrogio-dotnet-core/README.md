# C# (.NET Core) Version

The setup of a Dotnet Core project is pretty simple. We will use the [dotnet Core](https://dotnet.microsoft.com/) CLI utility that will do most of the work for us. It is compatible with all the main platforms (MacOS, PC, Linux) so it should be a painless setup with all the freedom needed.

## Installing Dotnet Core

1. Go to the dotnet core [download page](https://dotnet.microsoft.com/download).
2. Choose your platform.
3. Download the dotnet-sdk (version 2.2)
4. Follow the instructions at the download page for your platform.

## Building and executing the project

The steps to build your projects are really simple.

1. Open a terminal
2. Navigate to the `/path/of/Ambrogio.csproj` 
3. Run the command `dotnet run`

Please notice that the `dotnet run` command will do a bunch of things for you. Particularly, it will download eventual dependencies included in the project and it will start `Ambrogio`. 

## Setting up an IDE

There are several IDE you can work with C# and dotnet core. If you don't have `Visual Studio` already installed, we suggest to use `Visual Studio Code` (a.k.a. `Code`) that is lighter than the standard `Visual Studio`. 

1. Download `Code` from [here](https://code.visualstudio.com/).
2. Follow the wizard to have the IDE installed.
3. Open the IDE.
4. In the tab `Debug` select the last entry: `Install Additional Debuggers...`
5. It will open a panel on the left. Search for the `C#` debugger and install it.

**NOTE:** It is really important that you try and run your program also from the command line other than in the IDE. We are going to run it from there.

## Debugging on VSCode.

The debugger can be a little tricky to setup and use.
Follow those steps to set it up and to use it.

1. In the `Debug` tab, click on `Start Debugging`
2. Select `.NET Core`
3. Select the `Ambrogio` project
4. This will open a `launch.json` file
5. In the first `configurations` object, change the `console` value to `integratedTerminal`
6. Save the files.
7. Press F5 to start debugging.

The debugging process will build and run the app.
In the bottom panel it will open 4 different tabs: `Problems`, `Output`, `Debug Console`, `Terminal`.
The selected one will be the `Debug Console`. To perform I/O operation on the `Ambrogio`'s console, select the `Terminal` tab.

## Modifying the code

The main file you need to modify is `Ambrogio/plugins/Treasurer.cs`, in particular you need to provide an implementation for the methods `Init` (called when the program is started, giving you a chance to execute any initialization code needed) and `ReceiveMessage` (called every time a message is sent to the bot). Of course, you can add any other file and you can structure your code in the best way you can think of.

You'll have a `Bot` instance from the parent `Plugin` class available in Treasurer. It exposes the methods `StoreValue`, `RetrieveValue`, and `SendText`. The first two let you store and retrieve data by key, whereas the last one lets you send a message to Ambrogio's output stream. Feel free to read the documentation for those methods to better understand how they work.

The `Message` instance that you'll receive as a parameter to `ReceiveMessage ` has the following, self-explanatory properties:

- `Sender`,
- `Text`,
- `Date`.
    
## Adding dependencies

To add a dependencies, follow those steps:

1. Open a terminal.
2. Navigate to the `Ambrogio` folder.
3. Run the command `dotnet add package <name of the package>`.  
4. Run the command `dotnet restore` to let dotnet download the dependencies and build them.

**NOTE:** Please, note that we are going to use MacOS to test your code. So please, verify that the dependencies that you are adding are compatible with MacOS before using them.
